use bevy::prelude::*;

mod player;
use player::PlayerPlugin;

mod material;
mod settings;

mod chunk;
use chunk::plugin::ChunkPlugin;

mod blocks;

/// This example shows how to configure Physically Based Rendering (PBR) parameters.
fn main() {
    App::new()
        .add_startup_system(setup)
        .insert_resource(settings::Settings::default())
        .add_plugins(DefaultPlugins)
        .add_plugin(PlayerPlugin)
        .add_plugin(ChunkPlugin)
        .add_startup_system(material::add)
        .run();
}

fn setup(
    mut cmds: Commands,
) {
    cmds.insert_resource(WindowDescriptor {
        title: "Broxel".to_string(), 
        width: 1200.0, 
        height: 800.0, 
        ..default()
    });
}
