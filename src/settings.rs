use rand::thread_rng;
use rand::Rng;

pub struct Settings {
    pub chunk_gens_per_cylce: u16,
    pub chunk_meshes_per_cycle: u16,
    pub seed: u32,
}

impl Settings {
    pub fn default() -> Self {
        let mut rng = thread_rng();
        let seed = rng.gen();
        Settings {
            chunk_gens_per_cylce: 8,
            chunk_meshes_per_cycle: 4,
            seed
        }
    }
}