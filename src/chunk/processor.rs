// handles async Terrain and mesh generation of chunks
use bevy::prelude::*;
use bevy::tasks::AsyncComputeTaskPool;
use bevy::tasks::Task;

use super::ChunkData;
use super::LoadedChunk;
use super::SuperChunk;
use super::ChunkMap;
use super::VoxelMap;

use crate::settings::Settings;

use futures_lite::future;

pub struct ChunkMeshQueue {
    pub queue: Vec<[i64; 3]>,
    pub sorted: bool,
    pub last_player_chunk_pos: [i64; 3],
} impl ChunkMeshQueue {
    pub fn new() -> Self {
        Self {
            queue: Vec::new(),
            sorted: true,
            last_player_chunk_pos: [0, 0, 0]
        }
    }
}

pub struct ChunkGenQueue {
    pub queue: Vec<[i64; 3]>,
    pub sorted: bool,
    pub last_player_chunk_pos: [i64; 3],
} impl ChunkGenQueue {
    pub fn new() -> Self {
        Self {
            queue: Vec::new(),
            sorted: true,
            last_player_chunk_pos: [0, 0, 0]
        }
    }
}

#[derive(Component)]
pub struct MeshTask {
    pub task: Task<([i64; 3], Mesh)>
}

#[derive(Component)]
pub struct GenTask {
    pub task: Task<SuperChunk>
}

/* ASYNC MESH GENERATION */
pub fn prepare_mesh_tasks(
    mut queue: ResMut<ChunkMeshQueue>,
    chunks: Res<ChunkMap>,
    voxels: Res<VoxelMap>,
    pool: Res<AsyncComputeTaskPool>,
    mut cmds: Commands,
    settings: Res<Settings>,
) {
    if !queue.sorted {
        let pos = queue.last_player_chunk_pos;
        queue.queue.sort_unstable_by_key(|key| {
            (key[0] - pos[0]).abs() +
            (key[1] - pos[1]).abs() +
            (key[2] - pos[2]).abs()
        });
        queue.queue.reverse();
        queue.sorted = true;
    }

    for _ in 0..settings.chunk_meshes_per_cycle {
        if let Some(key) = queue.queue.pop() {
            if let Some(entity) = chunks.entity(key) {
                if let Some(data) = voxels.map.get(&key) {
                    let data = *data;
                    let task = pool.spawn(async move {
                        let mesh = super::mesh::generate(data);
                
                        (key, mesh)
                    });
        
                    cmds.entity(entity).insert(MeshTask{task});
                }
            }
        }
    }
}

pub fn apply_mesh_tasks(
    mut meshes: ResMut<Assets<Mesh>>,
    chunkmap: Res<ChunkMap>,
    mut query: Query<(
        &LoadedChunk,
        &Handle<Mesh>,
        &mut Visibility,
        &mut MeshTask,
    )>,
    mut cmds: Commands,
    settings: Res<Settings>,
) {
    let mut finished: usize = 0;
    for (chunk, handle, mut visibility, mut mesh_task) in query.iter_mut() {
        if let Some(entity) = chunkmap.entity(chunk.key) {
            if finished < settings.chunk_meshes_per_cycle as usize {
                if let Some( (_key, mesh) ) = future::block_on(future::poll_once(&mut mesh_task.task)) {
                    *meshes.get_mut(handle).unwrap() = mesh;
                    visibility.is_visible = true;
                    cmds.entity(entity).remove::<MeshTask>();
    
                    finished += 1;
                }
            }
        }
    };
}

/* ASYNC WORLD GENERATION */
pub fn prepare_gen_tasks(
    mut queue: ResMut<ChunkGenQueue>,
    chunks: Res<ChunkMap>,
    voxels: Res<VoxelMap>,
    pool: Res<AsyncComputeTaskPool>,
    mut cmds: Commands,
    settings: Res<Settings>,
) {
    if !queue.sorted {
        let pos = queue.last_player_chunk_pos;
        queue.queue.sort_unstable_by_key(|key| {
            (key[0] - pos[0]).abs() +
            (key[1] - pos[1]).abs() +
            (key[2] - pos[2]).abs()
        });
        queue.queue.reverse();
        queue.sorted = true;
    }

    let seed = settings.seed;
    for _ in 0..settings.chunk_gens_per_cylce {
        if let Some(key) = queue.queue.pop() {
            if let Some(entity) = chunks.entity(key) {
                let data = *voxels.map.get(&key).unwrap_or(&ChunkData::new());
                let task = pool.spawn(async move {
                    super::generation::generate(data, key, seed)
                });
    
                cmds.entity(entity).insert(GenTask{task});
            }
        }
    }
}

pub fn apply_gen_tasks(
    mut voxels: ResMut<VoxelMap>,
    mut query: Query<(Entity, &mut GenTask)>,
    mut mesh_queue: ResMut<ChunkMeshQueue>,
    gen_queue: Res<ChunkGenQueue>,
    mut cmds: Commands,
    settings: Res<Settings>,
) {
    let mut finished: usize = 0;
    for (entity, mut gen_task) in query.iter_mut() {
        if finished < settings.chunk_gens_per_cylce as usize {
            if let Some(super_chunk) = future::block_on(future::poll_once(&mut gen_task.task)) {
                for (key, data) in super_chunk.chunks.iter() {
                    //voxels.map.insert(*key, *data);
                    if let Some(voxels) = voxels.map.get_mut(key) {
                        voxels.merge(data);
                        mesh_queue.queue.push(*key);
                    } else {
                        voxels.map.insert(*key, *data);
                        mesh_queue.queue.push(*key);
                    }
                }
                cmds.entity(entity).remove::<GenTask>();
    
                mesh_queue.sorted = false;
                mesh_queue.last_player_chunk_pos = gen_queue.last_player_chunk_pos;
    
                finished += 1;
            }
        }
    }
}