use bevy::prelude::*;
use bevy::utils::HashMap;

use crate::material::Materials;

use super::CHUNK_SIZE;
use super::{RENDER_DISTANCE_HOR, RENDER_DISTANCE_VER};
use super::LoadedChunk;
use super::VoxelMap;

use super::ChunkMap;
pub struct ChunkPlugin;

use super::processor::ChunkMeshQueue;
use super::processor::ChunkGenQueue;

use super::ChunkUnloadQueue;

impl Plugin for ChunkPlugin {

    fn build(&self, app: &mut App) {
        app
            .insert_resource(ChunkMap(HashMap::default()))
            .insert_resource(ChunkMeshQueue::new())
            .insert_resource(ChunkGenQueue::new())
            .insert_resource(VoxelMap{map: HashMap::default()})
            .insert_resource(ChunkUnloadQueue{queue: Vec::new()})
            .add_system(super::processor::prepare_mesh_tasks.label("mesh_gen"))
            .add_system(super::processor::apply_mesh_tasks.label("mesh_gen"))
            .add_system(super::processor::prepare_gen_tasks.label("terr_gen"))
            .add_system(super::processor::apply_gen_tasks.label("terr_gen"))
            .add_system(chunk_spawner)
            .add_system(chunk_unload_detector)
            .add_system(
                chunk_unloader
                    .label("unloader")
                    .after("mesh_gen")
                    .after("terr_gen")
            )
            //.add_system(super::anim::chunk_spawn);
            .add_system(entity_count)
            .add_system(unloader);
    }
}

use crate::player::Player;

// spawns chunks around player when moved
fn chunk_spawner(
    mut query: Query<(&Transform, &mut Player)>,
    mut chunkmap: ResMut<ChunkMap>,
    mut queue: ResMut<ChunkGenQueue>,
    mut cmds: Commands,
    materials: Res<Materials>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    let mut player_pos = Vec3::ZERO;
    let mut player_moved: bool = false;
    for (transform, mut player) in query.iter_mut().next() {
        player_pos = transform.translation;
        if player.chunks_moved_since_load > 0 {
            player_moved = true;
            player.chunks_moved_since_load = 0;
        }
    }

    let chunk_x = (player_pos.x / CHUNK_SIZE as f32).round() as i64;
    let chunk_y = (player_pos.y / CHUNK_SIZE as f32).round() as i64;
    let chunk_z = (player_pos.z / CHUNK_SIZE as f32).round() as i64;

    // loading
    if player_moved {
        for x in -RENDER_DISTANCE_HOR..RENDER_DISTANCE_HOR {
            for z in -RENDER_DISTANCE_HOR..RENDER_DISTANCE_HOR {
                for y in -RENDER_DISTANCE_VER..RENDER_DISTANCE_VER {
                    // circular
                    if x.pow(2) + z.pow(2) >= RENDER_DISTANCE_HOR.pow(2) {
                        continue;
                    }

                    let chunk_x = chunk_x as i64 + x;
                    let chunk_y = chunk_y as i64 + y;
                    let chunk_z = chunk_z as i64 + z;
                    let chunk_coords = [chunk_x, chunk_y, chunk_z];

                    // check if chunk is not in world
                    if !chunkmap.contains_entity(&chunk_coords) {
                        let chunk = LoadedChunk::new(chunk_coords);
                        let entity = cmds.spawn()
                            .insert(chunk)
                            .insert_bundle(PbrBundle {
                                mesh: meshes.add(Mesh::from(shape::Cube {size: CHUNK_SIZE as f32 * 2.0})), // must have exact size
                                visibility: Visibility {is_visible: false},
                                transform: Transform::from_matrix(Mat4::from_scale_rotation_translation(
                                    Vec3::splat(1.0),
                                    Quat::from_rotation_x(0.0),
                                    Vec3::new((chunk_x * CHUNK_SIZE as i64) as f32, (chunk_y * CHUNK_SIZE as i64) as f32, (chunk_z * CHUNK_SIZE as i64) as f32),
                                )),
                                material: materials.chunk.clone(),
                                ..default()
                            })
                            .id();

                        chunkmap.attach_entity(chunk_coords, entity);
                        queue.queue.push(chunk_coords);
                    }
                }
            }
        }
        queue.sorted = false;
        queue.last_player_chunk_pos = [chunk_x, chunk_y, chunk_z];
    }
}

fn chunk_unload_detector(
    voxelmap: Res<VoxelMap>,
    mut query: Query<(&Transform, &mut Player)>,
    mut unload_queue: ResMut<ChunkUnloadQueue>,
) {
    let mut player_pos = Vec3::ZERO;
    let mut player_moved: bool = false;
    for (transform, mut player) in query.iter_mut().next() {
        player_pos = transform.translation;
        if player.chunks_moved_since_unload > 0 {
            player_moved = true;
            player.chunks_moved_since_unload = 0;
        }
    }

    let chunk_x = (player_pos.x / CHUNK_SIZE as f32).round() as i64;
    let chunk_y = (player_pos.y / CHUNK_SIZE as f32).round() as i64;
    let chunk_z = (player_pos.z / CHUNK_SIZE as f32).round() as i64;

    if player_moved {
        let coords = [chunk_x, chunk_y, chunk_z];

        voxelmap.map.keys().for_each(|key| {
            if (key[0] - coords[0]).abs() > RENDER_DISTANCE_HOR + 2
            || (key[1] - coords[1]).abs() > RENDER_DISTANCE_VER + 2
            || (key[2] - coords[2]).abs() > RENDER_DISTANCE_HOR + 2 {
                unload_queue.queue.push(*key);
            }
        });
    }
}

fn chunk_unloader(
    mut unload_queue: ResMut<ChunkUnloadQueue>,
    mut chunkmap: ResMut<ChunkMap>,
    mut voxelmap: ResMut<VoxelMap>,
    mut cmds: Commands,
) {
        for key in unload_queue.queue.drain(..) {
            if let Some(entity) = chunkmap.detach_entity(key) {
                cmds.entity(entity).despawn();
            }
            voxelmap.map.remove(&key);
        }
}

fn entity_count(
    query: Query<Entity>,
    mesh: Res<Assets<Mesh>>,
    loaded_chunks: Query<&LoadedChunk>,

    chunkmap: Res<ChunkMap>,
    voxelmap: Res<VoxelMap>,

    gen_queue: Res<ChunkGenQueue>,
    mesh_queue: Res<ChunkMeshQueue>,
    unload_queue: Res<ChunkUnloadQueue>,
) {
    let entities = query.iter().count();
    let meshes = mesh.iter().count();

    let chunks = chunkmap.0.len();
    let voxels = voxelmap.map.len();

    let mesh = mesh_queue.queue.len();
    let gen = gen_queue.queue.len();

    let unload = unload_queue.queue.len();
    let loaded_c = loaded_chunks.iter().count();

    println!("entities: {}\tmeshes: {}", entities, meshes);
    println!("chunkmap: {}\tvoxels: {}", chunks, voxels);
    println!("mesh_q:   {}\tgen_q:  {}", mesh, gen);
    println!("unload:   {}\tload_c: {}", unload, loaded_c);
    println!("");
}

fn unloader(
    mut cmds: Commands,
    loaded_chunks: Query<(Entity, &LoadedChunk)>,

    mut chunkmap: ResMut<ChunkMap>,
    mut voxelmap: ResMut<VoxelMap>,
    mut gen_queue: ResMut<ChunkGenQueue>,
    mut mesh_queue: ResMut<ChunkMeshQueue>,
    mut unload_queue: ResMut<ChunkUnloadQueue>,
    keys: Res<Input<KeyCode>>,
) {
    if keys.just_pressed(KeyCode::R) {
        for (entity, _) in loaded_chunks.iter() {
            cmds.entity(entity).despawn()
        }
        chunkmap.0.clear();
        voxelmap.map.clear();

        gen_queue.queue.clear();
        mesh_queue.queue.clear();
        unload_queue.queue.clear();
    }
}



