mod generation;
pub mod plugin;
mod processor;

mod mesh;

use bevy::prelude::*;
use bevy::utils::HashMap;
pub struct ChunkMap(HashMap<[i64; 3], Entity>);
impl ChunkMap {
    /// Returns the entity attached to the chunk.
    pub fn entity(&self, pos: [i64; 3]) -> Option<Entity> {
        self.0.get(&pos).map(|x| x.clone())
    }

    /// Attaches the specified entity to the chunk data.
    pub fn attach_entity(&mut self, pos: [i64; 3], entity: Entity) {
        self.0.insert(pos, entity);
    }

    /// Detaches the specified entity to the chunk data.
    pub fn detach_entity(&mut self, pos: [i64; 3]) -> Option<Entity> {
        self.0.remove(&pos)
    }

    pub fn contains_entity(&mut self, pos: &[i64; 3]) -> bool {
        self.0.contains_key(pos)
    }
}

pub struct VoxelMap{
    map: HashMap<[i64; 3], ChunkData>
}

use bevy::prelude::Component;

// does actually have 64bit limit because of casting
pub const CHUNK_SIZE: usize = 32; // cubic
pub const RENDER_DISTANCE_HOR: i64 = 8;
pub const RENDER_DISTANCE_VER: i64 = 4;

use crate::blocks::Block;

#[derive(Component)]
pub struct LoadedChunk {
    key: [i64; 3],
}
impl LoadedChunk {
    pub fn new(key: [i64; 3]) -> Self {
        Self {
            key,
        }
    }
}

use generation::structures::Structure;
// multiple chunks used for terrain generation over chunk borders
pub struct SuperChunk {
    chunks: HashMap<[i64; 3], ChunkData>,
} impl SuperChunk {
    pub fn new(main: ([i64; 3], ChunkData)) -> Self {
        let mut chunks = HashMap::default();
        chunks.insert(main.0, main.1);

        Self {
            chunks
        }
    }
    pub fn get(&self, coord: [i64; 3]) -> Option<Block> {
        let (chunk, index) = get_chunk_coords(&coord);
    
        if let Some(c) = self.chunks.get(&chunk) {
            return Some(c.data[index[0]][index[1]][index[2]])
        } else {
            return None
        } 
    }
    pub fn place(&mut self, coord: [i64; 3], block: Block) {
        if block != Block::None {
            let (chunk, index) = get_chunk_coords(&coord);
    
            if let Some(c) = self.chunks.get_mut(&chunk) {
                c.data[index[0]][index[1]][index[2]] = block;
            } else {
                let mut c = ChunkData::new();
                c.data[index[0]][index[1]][index[2]] = block;
                self.chunks.insert(chunk, c);
            }
        }
    }
    pub fn place_structure(&mut self, structure: &Structure, coord: [i64; 3]) {
        // to center structure on x and z
        let x_offset = structure.size[0] / 2;
        let z_offset = structure.size[2] / 2;

        for x in 0..structure.size[0] {
            for y in 0..structure.size[1] {
                for z in 0..structure.size[2] {
                    let block = structure.get([x, y, z]).unwrap();
                    if block != Block::None {
                        let x = coord[0] - x_offset as i64 + x as i64;
                        let y = coord[1] + y as i64;
                        let z = coord[2] - z_offset as i64 + z as i64;
    
                        self.place([x, y, z], block);
                    }
                }
            }
        }
    }
}

fn get_chunk_coords(coord: &[i64; 3]) -> ([i64; 3], [usize; 3]) {
    let chunk = [
        (coord[0] as f64 / CHUNK_SIZE as f64).floor() as i64,
        (coord[1] as f64 / CHUNK_SIZE as f64).floor() as i64,
        (coord[2] as f64 / CHUNK_SIZE as f64).floor() as i64
    ];
    let index = [
        (coord[0] - (chunk[0] * CHUNK_SIZE as i64)).abs() as usize,
        (coord[1] - (chunk[1] * CHUNK_SIZE as i64)).abs() as usize,
        (coord[2] - (chunk[2] * CHUNK_SIZE as i64)).abs() as usize
    ];

    (chunk, index)
}

pub struct ChunkUnloadQueue {
    queue: Vec<[i64; 3]>
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct ChunkData {
    pub data: [[[Block; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_SIZE],
}
impl ChunkData {
    pub fn new() -> Self {
        Self {
            data: [[[Block::None; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_SIZE],
        }
    }
    pub fn merge(&mut self, other: &Self) {
        for x in 0..CHUNK_SIZE {
            for y in 0..CHUNK_SIZE {
                for z in 0..CHUNK_SIZE {
                    if other.data[x][y][z] != Block::None {
                        self.data[x][y][z] = other.data[x][y][z];
                    }
                }
            }

        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Chunk {
    key: [i64; 3],

    pub data: ChunkData
}